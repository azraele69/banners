<?php
const PATH_TO_IMAGE = 'banner_img.jpg';

const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = 'newpass';
const DB_DATABASE = 'banners';

function getFormattedDate(): string
{
    return (new DateTime('now'))->format("Y-m-d H:i:s");
}

function DBConnection(): PDO
{
    $conn = new PDO(sprintf("mysql:host=%s;dbname=%s",DB_HOST, DB_DATABASE), DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
}

function createView (PDO $conn, string $ip_address, string $user_agent, string $view_date, string $page_url): void
{
    $stmt = $conn->prepare("INSERT INTO views (ip_address, user_agent, view_date, page_url) 
                                       VALUES (:ip_address, :user_agent, :view_date, :page_url)");
    $stmt->bindParam(':ip_address', $ip_address);
    $stmt->bindParam(':user_agent', $user_agent);
    $stmt->bindParam(':view_date', $view_date);
    $stmt->bindParam(':page_url', $page_url);

    $stmt->execute();
}

function getViewByParams (PDO $conn, string $ip_address, string $user_agent, string $page_url)
{
    $stmt = $conn->prepare("SELECT * FROM views WHERE ip_address = :ip_address AND  user_agent = :user_agent AND page_url = :page_url");
    $stmt->bindParam(':ip_address', $ip_address);
    $stmt->bindParam(':user_agent', $user_agent);
    $stmt->bindParam(':page_url', $page_url);
    $stmt->execute();

    return $stmt->fetch();
}

function updateView (PDO $conn, array $view): void
{
    $now = getFormattedDate();
    $stmt = $conn->prepare("UPDATE views SET views_count=:views_count + 1, view_date=:view_date WHERE id=:id");
    $stmt->bindParam(':views_count', $view['views_count']);
    $stmt->bindParam(':view_date', $now);
    $stmt->bindParam(':id', $view['id']);
    $stmt->execute();
}

echo PATH_TO_IMAGE;

try {
    $connection = DBConnection();
    $ip_address =  $_SERVER['REMOTE_ADDR'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $view_date = getFormattedDate();
    $page_url =  $_SERVER['REQUEST_URI'];

    $view = getViewByParams($connection, $ip_address, $user_agent, $page_url);

    if ($view) {
        updateView($connection, $view);
    } else {
        createView($connection, $ip_address, $user_agent, $view_date, $page_url);
    }
} catch(Throwable $e) {
    ///TODO .... if it's necessary
}



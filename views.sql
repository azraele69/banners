 CREATE TABLE `views` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`ip_address` VARCHAR(45) NOT NULL,
	`user_agent` VARCHAR(255) NOT NULL,
	`view_date` DATETIME NOT NULL,
	`page_url` VARCHAR(255) NOT NULL,
	`views_count` INT(11) NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
)
